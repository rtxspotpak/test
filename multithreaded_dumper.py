import threading
import dumper
import concurrent.futures
import logging
import time

class MultithreadDumperTask(threading.Thread):
    output_folder = None
    corelink = None
    id = 0
    def __init__(self, output_folder, corelink, id):
        threading.Thread.__init__(self)
        self.output_folder = output_folder
        self.corelink = corelink
        self.id = id

    def multithread_dumper(self, output_folder, corelink, id):
        new_link = corelink + str(id)
        print("Accessing", id, "->", new_link)
        data = dumper.dump(new_link)
        if data:
            print("Dumping", new_link)
            file = open(output_folder + 'sigaa_' + str(id) + '.txt', 'w+')
            file.write(str(data.name))
            file.write('\n')
            file.write(str(data.level))
            file.write('\n')
            file.write(str(data.place))
            file.write('\n')
            file.write(str(data.students))
            file.close()
            print("End of dump:", new_link)

    def run(self):
        self.multithread_dumper(self.output_folder, self.corelink, self.id)

if __name__ == "__main__":
    i = 0
    max = 100000
    tmax = 8
    t = []
    wb = []
    while i < max:
        while len(t) < tmax:
            _titem = MultithreadDumperTask(dumper.output_folder, dumper._corelink, i)
            t.append(_titem)
            i += 1
        while len(t):
            _titem = t[len(t) - 1]
            _titem.start()
            wb.append(_titem)
            t.remove(_titem)
        while len(wb):
            _titem = wb[len(wb)-1]
            _titem.join()
            wb.remove(_titem)