import mechanize
import copy
from bs4 import BeautifulSoup
from bs4 import Tag

_corelink = "https://sigaa.ufpi.br/sigaa/public/curso/alunos.jsf?lc=pt_BR&id="
output_folder = 'dump/'

class CourseDump:
    valid = False
    name = None
    level = None
    place = None
    students = None

    def __init__(self):
        pass
    def set_name(self, name):
        self.name = name
    def set_level(self, level):
        self.level = level
    def set_place(self, place):
        self.place = place
    def set_student_list(self, students):
        self.students = students
    def get_valid(self):
        if self.name and self.level and self.place and self.students:
            self.valid = True
            return self
        else:
            return None


def parse_course_data(link):
    browser = mechanize.Browser()
    html_doc = browser.open(link)
    soup = BeautifulSoup(html_doc.read(), 'html.parser')
    course_name = None
    course_level = None
    course_place = None
    data = soup.find('form', id='formIdioma')
    course_name = data.find('h1')
    course_level = data.find('h2')
    course_place = data.find_all('a')[1]
    course_name = course_name.get_text().replace('\n','').replace('\r','').replace('\t', '').replace('\xa0', '')
    course_level = course_level.get_text().replace('\n','').replace('\r','').replace('\t', '').replace('\xa0', '')
    course_place = course_place.get_text().replace('\n','').replace('\r','').replace('\t', '').replace('\xa0', '')
    return course_name, course_place, course_level

def parse_student_list(link):
    browser = mechanize.Browser()
    html_doc = browser.open(link)
    soup = BeautifulSoup(html_doc.read(), 'html.parser')
    alunos = []
    new_item = {}
    temporary_id = ''
    for i in soup.find_all('td'):
        if i.get('class'):
            if i.get('class')[0] == 'colMatricula':
                temporary_id = i.get_text()
                new_item[temporary_id] = None
        else:
            if (i.get('colspan')):
                continue
            else:
                new_item[temporary_id] = i.get_text()
                if new_item[temporary_id]:
                    alunos.append(new_item.copy())
                new_item = {}
    return alunos

def dump(link):
    course_data = parse_course_data(link)
    students = parse_student_list(link)
    c = CourseDump()
    c.set_name(course_data[0])
    c.set_level(course_data[1])
    c.set_place(course_data[2])
    c.set_student_list(students)
    return c.get_valid()

def dump_all_sigaa_database(corelink):
    id = 1
    max = 100000
    while id <= max:
        new_link = corelink + str(id)
        print("Accessing", new_link)
        data = dump(new_link)
        if data:
            print("Dumping", new_link)
            file = open(output_folder + 'sigaa_' + str(id) + '.txt', 'w+')
            file.write(str(data.name))
            file.write('\n')
            file.write(str(data.level))
            file.write('\n')
            file.write(str(data.place))
            file.write('\n')
            file.write(str(data.students))
            file.close()
        id += 1